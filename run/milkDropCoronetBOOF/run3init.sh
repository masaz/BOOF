#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture-o
#PJM -L node=2
#PJM -L elapse=0:15:00
#PJM --mpi proc=96
#PJM -j
#PJM -S
source ./runShare.sh
restore0Dir -processor
mpiexec -of log.setFields.$jid setFields -parallel
