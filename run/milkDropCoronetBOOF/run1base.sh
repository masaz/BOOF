#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture-o
#PJM -L node=1
#PJM -L elapse=0:15:00
#PJM --mpi proc=1
#PJM -j
#PJM -S
source ./runShare.sh
blockMesh &> log.blockMesh.$jid
decomposePar -cellDist &> log.decomposePar.$jid
