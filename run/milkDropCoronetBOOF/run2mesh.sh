#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture-o
#PJM -L node=2
#PJM -L elapse=0:15:00
#PJM --mpi proc=96
#PJM -j
#PJM -S
source ./runShare.sh
mpiexec -of log.snappyHexMesh.$jid snappyHexMesh -overwrite -parallel
mpiexec -of log.renumberMesh.$jid renumberMesh -overwrite -parallel
mpiexec -of log.checkMesh.$jid checkMesh -constant -parallel
