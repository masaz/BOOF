#!/bin/bash
#PJM -g gt00
#PJM -L rscgrp=lecture-o
#PJM -L node=2x2x2:mesh
#PJM -L elapse=0:15:00
#PJM --mpi proc=384
#PJM -j
#PJM -S
source ./runShare.sh
mpiexec -of log.$application.$jid $application -parallel
