#!/usr/bin/gnuplot -persist
set terminal pdf font "times,18"
set output "diameter.pdf"
set xrange [ 0 :  * ]
set yrange [ 0 :  * ]
set grid
set xlabel "Time [ms]"
set ylabel "Diameter [cm]"
set key left
plot "plot/exp.txt" using 1:2 with lp title "Exp."\
     , "diameter.txt" using 1:2 with l title "CFD"\

