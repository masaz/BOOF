#!/usr/bin/env python3
import sys
import pandas as pd


def calcRMSE(dfExp, dfCFD):
    df = pd.merge(dfCFD, dfExp, how="outer", on='x')
    df = df.sort_values("x")
    df0 = df.rename(columns={'Exp': 'SquareError', 'x': 'Time'})
    df0 = df0.drop(columns='CFD',axis=1)
    df = df.set_index('x', drop=False)
    df = df.rename(columns={'x': 'Time'})
    df = df.interpolate(method='values',limit_area='inside')
    df = pd.merge(df, df0, how="outer", on='Time')
    df = df.dropna(how='any')
    df['SquareError']= (df['Exp']-df['CFD'])**2
    RMSE = df['SquareError'].mean()**0.5
    n = len(df)
    return(n, RMSE)


def main():
    if len(sys.argv) != 3:
        print(f'Usage: {sys.argv[0]} Exp_filename CFD_filename', file=sys.stderr)
        exit(0)

    dfExp = pd.read_table(sys.argv[1],sep=' ', header=None, names=('x', 'Exp'), skiprows=1, dtype='float')
    dfCFD = pd.read_table(sys.argv[2],sep=' ', header=None, names=('x', 'CFD'), skiprows=1, dtype='float')
    nCFD = len(dfCFD)

    if nCFD==0:
        exit(0)

    nOrig, RMSEOrig = calcRMSE(dfExp, dfCFD)

    x1 = 1e-10
    x2 = 1e+10
    if nCFD>1:
        x1 = 2*dfCFD['x'][nCFD-1]-dfCFD['x'][nCFD-2]
    dfCFD = dfCFD.append({'x' :  x1, 'CFD' : 0}, ignore_index=True)
    dfCFD = dfCFD.append({'x' :  x2, 'CFD' : 0}, ignore_index=True)
    n, RMSE = calcRMSE(dfExp, dfCFD)

    print(nOrig,RMSE)


if __name__ == '__main__':
    main()
