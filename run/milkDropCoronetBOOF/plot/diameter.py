#!/usr/bin/env python3
import sys
import os
import glob

def calcDiamiter(surfaceAlpha, time, sample_filename):
    alphaOld=1
    xOld=0
    diameter=0
    impact=False
    with open(time+'/'+sample_filename) as f:
        lines = f.readlines()    
    for line in reversed(lines):     
        x=float(line.split('\t')[0])
        alpha=float(line.split('\t')[1])
        if alpha>surfaceAlpha:
            impact=True
            r=(alpha-surfaceAlpha)/(alpha-alphaOld)
            diameter=((1-r)*x+r*xOld)*2.0
            break

        alphaOld=alpha
        xOld=x
    return impact, diameter

if len(sys.argv) < 3:
    print(f'Usage: {sys.argv[0]} surfaceAlpha sample_filename [sample_filenames...]', file=sys.stderr)
    exit(0)

surfaceAlpha=float(sys.argv[1])
    
fout = open('diameter.txt', 'w')

try:
    cd = os.chdir("postProcessing/sample/")
except FileNotFoundError:
    exit(0)

fout.write('#Time[ms] Diameter(CFD)[cm]\n')

times=sorted(glob.glob('[0-9]*'), key=float)
t0=False
for time in times:
    n_impact=0
    mean_diameter=0.0
    for sample_filename in sys.argv[2:]:
        impact, diamter = calcDiamiter(surfaceAlpha, time, sample_filename)
        if impact:
            if not t0:
                t0=time
            n_impact+=1
            mean_diameter+=diamter
    if n_impact>0:
        mean_diameter/=n_impact
        fout.write(f'{(float(time)-float(t0))*1000.0:g} {mean_diameter*100.0:g}\n')

fout.close()
print(t0)
