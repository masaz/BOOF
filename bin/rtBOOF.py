#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import optuna
import sys
import socket
import argparse
import shlex
import time
from collections import OrderedDict
import json
import re
import pathlib
import os
import subprocess
import numpy as np
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile

def parseOptions():
    """
    Parse options
    """
    p = argparse.ArgumentParser(
        description='Run-time bayesian optimization of solver settings for OpenFOAM solver.')

    p.add_argument('logFilename', help='log filename of solver')
    p.add_argument('--debug', help='Debug mode', type=int, default=0)
    p.add_argument('--interval', help='Interval', type=int, default=1)
    p.add_argument('--useStorage', help='useStorage', action='store_true')
    p.add_argument('--study', help='study name', type=str, default='BOOF')
    p.add_argument('--storage', help='storage',
                   type=str, default='sqlite:///BOOF.db')
    p.add_argument('--seed', help='Seed number' ,type=int, default=13)
    p.add_argument('--json', help='Json filename', type=str)
    p.add_argument('--nTimes', help='Number of initial steps to perse log file', type=int, default=5)
    p.add_argument('--maxIterp', help='Maximum number of of trials', type=int, default=5000)
    p.add_argument('--noRestoreBest', help='Do not restore best fvSoluton file when finished', action='store_true')
    p.add_argument('--nTrials', help='Maximum number of of trials', type=int, default=-1)
    p.add_argument('--perDeltaTime', help='Minimumize execution time per delta time', action='store_true')
    p.add_argument('--noUsePCG', help='Do not use PCG solver', action='store_true')
    p.add_argument('--noUseGAMG', help='Do not use GAMG solver', action='store_true')
    p.add_argument('--noUsePPCR', help='Do not use PPCR solver', action='store_true')
    p.add_argument('--noUsePPCG', help='Do not use PPCG solver', action='store_true')
    p.add_argument('--noUsePBiCG', help='Do not use PBiCGStab solver', action='store_true')
    p.add_argument('--noUsesmoothSolver', help='Do not use smoothSolver solver', action='store_true')
    p.add_argument('--noUsePBiCGStab', help='Do not use PBiCGStab solver', action='store_true')
    p.add_argument('--earlyStoppingTrials', type=int, default=100)

    return p.parse_args()


def parseConfig(filename):
    with open(filename) as f:
        df = json.load(f, object_pairs_hook=OrderedDict)

    return(df)


def parseLogAndGenerateConfig():
    df = OrderedDict()
    df['system/fvSolution'] = OrderedDict()
    p = re.compile(
        r'(.*):\s+Solving for (.*), Initial residual = (.*), Final residual = (.*), No Iterations (.*)\n')
    nTimes = 0
    PIMPLE = False
    fieldIterations = {}
    fieldSolver = {}
    skipFields = re.compile(r'(ILambda_.*|pcorr|cellDisplacement[xyz])')
    while True:
        line = logfile.readline()
        if args.debug >= 2:
            print(line.rstrip())
        if line.startswith('End'):
            print('Solver has already finished. Terminate.')
            exit()
        if re.match(r'^Time = .*\n', line):
            nTimes = nTimes+1
            if nTimes > args.nTimes:
                break
        if nTimes == 0:
            continue

        if line.startswith('PIMPLE:'):
            PIMPLE = True

        m = p.match(line)
        if m:
            field = m.groups()[1]
            if skipFields.match(field):
                continue
            solver = m.groups()[0]
            fieldSolver[field] = solver
            NoIterations = int(m.groups()[4])
            if field not in fieldIterations:
                fieldIterations[field] = 0
            fieldIterations[field] = fieldIterations[field]+NoIterations

    symmetricMatrixSolver = re.compile(r'(.*GAMG|.*PCG|.*PPCG|.*PPCR)')
    UField = re.compile(r'(U.*x)')
    UyzField = re.compile(r'(U.*[yz])')
    for field, NoIterations in fieldIterations.items():
        if NoIterations > 0:
            solver = fieldSolver[field]
            # symmetry matrix solver
            if symmetricMatrixSolver.match(solver):
                # skip kinematicCloud
                if field.startswith('kinematicCloud:'):
                    continue
                if PIMPLE:
                    fs = [field, field+'Final']
                else:
                    fs = [field]

                for f in fs:
                    solverName = f'solvers/{f}/solver'
                    df['system/fvSolution'][solverName] = []

                    if not args.noUsePCG:
                        df['system/fvSolution'][solverName].append(
                            OrderedDict([('PCG', OrderedDict([
                                (f'solvers/{f}/preconditioner', [
                                    'DIC',
                                    'FDIC',
                                    OrderedDict([
                                        ('', OrderedDict([
                                            (f'solvers/{f}/preconditioner/preconditioner',
                                             ['GAMG']),
                                            (f'solvers/{f}/preconditioner/smoother', [
                                                'DIC', 'DICGaussSeidel', 'FDIC', 'GaussSeidel', 'symGaussSeidel']),
                                            (f'solvers/{f}/preconditioner/agglomerator',
                                             ['faceAreaPair', 'algebraicPair']),
                                            (f'solvers/{f}/preconditioner/cacheAgglomeration',
                                             ['true', 'false']),
                                            (f'solvers/{f}/preconditioner/scaleCorrection',
                                             ['true', 'false']),
                                            (f'solvers/{f}/preconditioner/nCellsInCoarsestLevel',
                                             [5, 10, 20, 40, 80, 160, 320])
                                        ]))])])]))])
                        )


                    if not args.noUseGAMG:
                        df['system/fvSolution'][solverName].append(
                            OrderedDict([('GAMG', OrderedDict([
                                (f'solvers/{f}/smoother',
                                ['DIC', 'DICGaussSeidel', 'FDIC', 'GaussSeidel', 'symGaussSeidel']),
                                (f'solvers/{f}/agglomerator',
                                 ['faceAreaPair', 'algebraicPair']),
                                (f'solvers/{f}/cacheAgglomeration',
                                 ['true', 'false']),
                                (f'solvers/{f}/scaleCorrection',
                                 ['true', 'false']),
                                (f'solvers/{f}/nCellsInCoarsestLevel', [5, 10, 20, 40, 80, 160, 320])]))])
                        )
                    if not args.noUsePPCR:
                        df['system/fvSolution'][solverName].append(
                            OrderedDict([('PPCR', OrderedDict([
                                (f'solvers/{f}/preconditioner', ['DIC', 'FDIC'])]))])
                        )
                    if not args.noUsePPCG:
                        df['system/fvSolution'][solverName].append(
                            OrderedDict([('PPCG', OrderedDict([
                                (f'solvers/{f}/preconditioner', ['DIC', 'FDIC'])]))])
                        )

            else:
                # asymmetry matrix solver
                if UyzField.match(field):
                    continue
                if UField.match(field):
                    f = field.rstrip('x')
                else:
                    f = field

                if PIMPLE:
                    f = f+'Final'

                solverName = f'solvers/{f}/solver'

                df['system/fvSolution'][solverName] = []

                if not args.noUsePBiCG:
                    df['system/fvSolution'][solverName].append(
                    OrderedDict([('PBiCG', OrderedDict([
                        (f'solvers/{f}/preconditioner', ['DILU'])]))]))

                if not args.noUsesmoothSolver:
                    df['system/fvSolution'][solverName].append(
                    OrderedDict([('smoothSolver', OrderedDict([
                        (f'solvers/{f}/smoother', ['GaussSeidel', 'symGaussSeidel'])]))]))

                if not args.noUsePBiCGStab:
                    df['system/fvSolution'][solverName].append(
                    OrderedDict([('PBiCGStab', OrderedDict([
                        (f'solvers/{f}/preconditioner', ['DILU'])]))]))
                    
    return(df)


def follow():
    while True:
        line = logfile.readline()
        if not line:
            time.sleep(0.1)
            continue
        yield line


def executionTimePerStep(study, trial, config, paramFileList):
    reRead = [False] * len(config)
    n = 0
    t = []
    time = []
    timeStart = False
    logfile.seek(0, 2)
    for i, filename in enumerate(config):
        paramFileList[i].writeFile()
        pathlib.Path(filename).touch(exist_ok=True)
        if args.debug >= 1:
            print(f'# Update {filename}')
    for line in follow():
        if args.debug >= 2:
            print(line.rstrip())

        if args.debug >= 3:
            print(f'# n={n}, t={t}')

        if line.startswith('End'):
            return(-1)

        if all(reRead):
            if re.match(r'^Time = .*\n', line):
                time.append(float(line.split(' ')[2]))
                timeStart = True
            if timeStart and re.match(r'^ExecutionTime = .*\n', line):
                t.append(float(line.split(' ')[2]))
                n = n+1
                if (n > args.interval):
                    break
        else:
            if re.match(r'^Time = .*\n', line):
                for filename in config:
                    pathlib.Path(filename).touch(exist_ok=True)
                    if args.debug >= 2:
                        print('# Update time-stamp of {filename}')
                continue

            for i, filename in enumerate(config):
                basename = os.path.basename(filename)
                str = f'        Re-reading object {basename} '
                if line.startswith(str):
                    reRead[i] = True
                    if args.debug >= 1:
                        print(f'# Update confirmed {filename}')

    if args.perDeltaTime:
        tave = (t[args.interval]-t[0])/(time[args.interval]-time[0])
    else:
        tave = (t[args.interval]-t[0])/float(args.interval)
    if args.debug >= 1:
        print(f'# tave={tave}')

    return(tave)


def suggest(subConfig, trial, paramFile):
    for keys in subConfig:
        valI = trial.suggest_int(keys, 0, len(subConfig[keys])-1)
        val = subConfig[keys][valI]
        if isinstance(val, dict):
            value = list(val.keys())[0]
        else:
            value = val

        if args.debug >= 1:
            print(f'keys:{keys} value={value}')

        keysList = keys.split('/')
        paramFileTmp = paramFile
        n = len(keysList)
        for keyI in range(n):
            key = keysList[keyI]
            if keyI == (n-1):
                if isinstance(val, dict):
                    if value == '':
                        paramFileTmp[key] = {}
                    else:
                        paramFileTmp[key] = value
                    subsubConfig = list(val.values())[0]
                    suggest(subsubConfig, trial, paramFile)
                else:
                    paramFileTmp[key] = value
            else:
                paramFileTmp = paramFileTmp[key]

    return


def main():
    print(f'Host: {socket.gethostname()}')
    print(f'PID: {os.getpid()}')

    global args
    args = parseOptions()

    if subprocess.run(
            'nm $FOAM_LIBBIN/libOpenFOAM.so | grep -q addPPCRToDebug'
            , shell=True).returncode != 0:
        args.noUsePPCR = True
        
    if subprocess.run(
            'nm $FOAM_LIBBIN/libOpenFOAM.so | grep -q addPPCGToDebug'
            , shell=True).returncode != 0:
        args.noUsePPCG = True
        
    if subprocess.run(
            'nm $FOAM_LIBBIN/libOpenFOAM.so | grep -q addPBiCGToDebug'
            , shell=True).returncode != 0:
        args.noUsePBiCG = True
        
    if subprocess.run(
            'nm $FOAM_LIBBIN/libOpenFOAM.so | grep -q addsmoothSolverToDebug'
            , shell=True).returncode != 0:
        args.noUsesmoothSolver = True
        
    if subprocess.run(
            'nm $FOAM_LIBBIN/libOpenFOAM.so | grep -q addPBiCGStabToDebug'
            , shell=True).returncode != 0:
        args.noUsePBiCGStab = True
        
    while True:
        if os.path.isfile(args.logFilename):
            break
        time.sleep(0.1)

    global logfile
    logfile = open(args.logFilename, 'r')

    global config
    if args.json is not None:
        config = parseConfig(args.json)
    else:
        config = parseLogAndGenerateConfig()
        with open('BOOF-dump.json', 'w') as f:
            json.dump(config, f, indent=4)
        if len(config['system/fvSolution']) == 0:
            print('No fields to optimize. Terminate.')
            exit()

    if args.useStorage:
        storage = args.storage
    else:
        storage = None

    study = optuna.create_study(
        study_name=args.study,
        storage=storage,
        sampler=optuna.samplers.TPESampler(seed=args.seed),
        load_if_exists=True
    )

    # Optimize objective function
    ntrial = 0
    iter = 0 
    score = np.inf
    while True:
        ntrial += 1
        if (args.nTrials >=0 and ntrial > args.nTrials):
            break

        trial = study.ask()

        paramFileList = []
        for filename in config:
            paramFile = ParsedParameterFile(filename)
            suggest(config[filename], trial, paramFile)
            if filename == 'system/fvSolution':
                for field in ['p', 'pFinal', 'p_rgh', 'p_rghFinal']:
                    if field in paramFile['solvers']:
                        paramFile['solvers'][field]['maxIter'] = args.maxIterp
            paramFileList.append(paramFile)
    
        tave = executionTimePerStep(study, trial, config, paramFileList)

        if tave < 0:
            print('Solver ends. Terminate.')
            break
    
        study.tell(trial, tave)

        print(f'Trial {trial.number} finished with value: {tave:g} and parameters: {trial.params}. '
                f'Best is trial {study.best_trial.number} with value: {study.best_value:g}.')

        if study.best_value < score:
            iter = 0
            score = study.best_value
        else:
            iter += 1

        if iter >= args.earlyStoppingTrials:
            print(f'Best value has not been updated for {iter} trials. Terminate.')
            break

    pruned_trials = [t for t in study.trials if t.state ==
                     optuna.trial.TrialState.PRUNED]
    timeout_trials = [t for t in study.trials if t.state ==
                      optuna.trial.TrialState.FAIL]
    complete_trials = [t for t in study.trials if t.state ==
                       optuna.trial.TrialState.COMPLETE]
    print('Study statistics: ')
    print('  Number of finished trials: ', len(study.trials))
    print('  Number of pruned trials: ', len(pruned_trials))
    print('  Number of timeout trials: ', len(timeout_trials))
    print('  Number of complete trials: ', len(complete_trials))

    trial = study.best_trial
    print(f'Best trial value: {trial.value:g}')

    if not args.noRestoreBest:
        print('Restore best fvSoluton file.')
        paramFileList = []
        for filename in config:
            paramFile = ParsedParameterFile(filename)
            suggest(config[filename], trial, paramFile)
            if filename == 'system/fvSolution':
                for field in ['p', 'pFinal', 'p_rgh', 'p_rghFinal']:
                    if field in paramFile['solvers']:
                        paramFile['solvers'][field]['maxIter'] = args.maxIterp
            paramFileList.append(paramFile)

        for i, filename in enumerate(config):
            paramFileList[i].writeFile()
        pathlib.Path(filename).touch(exist_ok=True)


if __name__ == '__main__':
    main()
