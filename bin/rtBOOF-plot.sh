#!/bin/bash
# Tutorial run functions
. ${WM_PROJECT_DIR:?}/bin/tools/RunFunctions

# Appilication name
application=$(getApplication)

for dir in . ..
do
    time=$dir/time.txt
    awk '/^Time =/ {} /^Time =/ {t=$3} /^ExecutionTime / {print t,$3,$7}' $dir/log.${application}* > $time
done

GPFILE=$(mktemp)
cat > $GPFILE <<'EOF'
set terminal pdf lw 3 font "Helvetica,16"
set style data line
set ylabel "Execution time [s]" 
set key left

set output 'ExecutionTimeCaseStep.pdf'
set xlabel "Time steps" 
plot \
     "time.txt" using 0:2 title 'Optimize'\
EOF

[ -f ../time.txt ]  && cat >> $GPFILE <<'EOF'
,"../time.txt" using 0:2 title 'Original'
EOF

cat >> $GPFILE <<'EOF'

set output 'ExecutionTimeCaseTime.pdf'
set xlabel "Time [s]" 
plot \
     "time.txt" using 1:2 title 'Optimize'\
EOF

[ -f ../time.txt ]  && cat >> $GPFILE <<'EOF'
,"../time.txt" using 1:2 title 'Original'
EOF

gnuplot $GPFILE
