#!/bin/bash

Script="${0##*/}"

usage() {
    exec 1>&2
    while [ "$#" -ge 1 ]; do echo "$1"; shift; done
    cat <<USAGE

Usage: $Script [OPTIONS] <directory>
  -h                    print the usage

$Script - Make case directory for rtBOOF.

USAGE
    exit 1
}

# Parse options
while [ "$#" -gt 0 ]
do
    case "$1" in
    -h)
        printHelp
        exit 0
        ;;
    -*)
        usage "unknown option: '$1'"
        ;;
    *)
        break
        ;;
    esac
    shift
done


# Requires a single argument
[ $# -eq 1 ] || usage

dir=$1

[ ! -d $dir ] && mkdir $dir
cd $dir
ln -s ../{0,constant} ./
mkdir system
(cd system
    ln -s ../../system/* ./
    rm -f fvSolution controlDict
    [ ! -f decomposeParDict ] && ln -s decomposeParDict* decomposeParDict
)
(
  cd ..
  foamDictionary system/fvSolution
) > system/fvSolution
cp -a system/fvSolution system/fvSolution.orig
(
  cd ..
  foamDictionary system/controlDict
  cat <<EOF
runTimeModifiable yes;
OptimisationSwitches
{
  fileModificationSkew 1;
  maxFileModificationPolls 100;
}
EOF
) > system/controlDict
for p in $(cd ..;echo processor*)
do 
    mkdir $p
    (
	cd $p
	ln -s ../../$p/{0,constant} .
    )
done
