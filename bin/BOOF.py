#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import hashlib
import itertools
import json
import numpy
import os
import optuna
import pathlib
import socket
import shutil
import subprocess
import sys
import time
from collections import OrderedDict
from PyFoam.RunDictionary.ParsedParameterFile import ParsedParameterFile
from optuna.pruners import SuccessiveHalvingPruner
from optuna.exceptions import TrialPruned

def parseOptions():
    """
    Parse options
    """
    p = argparse.ArgumentParser()

    p.add_argument('--debug', type=int, default=0)
    p.add_argument('--nJobs', type=int, default=1)
    p.add_argument('--nTrials', type=int, default=10)
    p.add_argument('--nJobTrials', type=int, default=1)
    p.add_argument('--study', type=str, default='BOOF')
    p.add_argument('--useStorage', action='store_true')
    p.add_argument('--storage', type=str, default='sqlite:///BOOF.db')
    p.add_argument('--seed', type=int, default=10)
    p.add_argument('--json', type=str, default='BOOF.json')
    p.add_argument('--baseDir', type=str, default='cases')
    p.add_argument('--useDefaultFirst', action='store_true')
    p.add_argument('--useGridSearch', action='store_true')
    p.add_argument('--prune', action='store_true')

    return p.parse_args()


def parseConfig(filename):
    """
    Read JSON format configuration file
    """
    with open(filename) as f:
        df = json.load(f, object_pairs_hook=OrderedDict)

    return(df)


def submitJob(trial):
    """
    Submit job
    """
    sortedParames = {key: trial.params[key]
                     for key in sorted(trial.params.keys())}
    paramsStr = "{}".format(sortedParames)
    case = "_"+hashlib.sha1(paramsStr.encode('utf-8')).hexdigest()
    casePath = pathlib.Path(args.baseDir) / case
    casePath.mkdir(exist_ok=True)
    with open(casePath / 'BOOF.params', 'w') as f:
        f.write(paramsStr)

    # copy cases
    command = ["../../BOOF.copy"]
    print('Execute job {} at {}'.format(
        ' '.join(command), casePath), file=sys.stderr)
    jobid = 0
    try:
        command_output = subprocess.check_output(
            command,
            cwd=casePath,
            universal_newlines=True
        )
    except subprocess.CalledProcessError as e:
        print('Error in job {} at {}'.format(
            ' '.join(command), casePath), file=sys.stderr)

    paramFileTmp = None
    filenameOld = None
    for filenameKeys, value in trial.params.items():
        filename, keys = filenameKeys.split(':')
        keysList = keys.split('/')
        if filename != filenameOld:
            if filenameOld != None:
                paramFile.writeFile()
            paramFile = ParsedParameterFile(casePath / filename)
            filenameOld = filename
        paramFileTmp = paramFile
        n = len(keysList)
        for keyI in range(n):
            key = keysList[keyI]
            if keyI == (n-1):
                paramFileTmp[key] = value
            else:
                paramFileTmp = paramFileTmp[key]
    paramFile.writeFile()

    # run solver
    command = "exec ../../BOOF.run"
    print('Execute job {} at {}'.format(command, casePath), file=sys.stderr)
    jobid = 0
    try:
        p = subprocess.Popen(
            command,
            cwd=casePath,
            shell=True
        )
    except subprocess.CalledProcessError as e:
        print('Error in job {} at {}'.format(command, casePath), file=sys.stderr)
        return numpy.nan

    logFilename=casePath / 'log.BOOF.run'
    epoch=1
    while True:
        if args.prune:        
            reportFilename=casePath / 'log.BOOF.run.report.{}'.format(epoch)
            if  os.path.isfile(reportFilename):
                with open(reportFilename) as reportf:
                    objectFunction=float(reportf.readline())
                    trial.report(objectFunction, epoch)
                    epoch+=1
                    if trial.should_prune():
                        raise optuna.exceptions.TrialPruned()
        if os.path.isfile(logFilename):
            with open(logFilename) as logf:
                objectFunction=float(logf.readline())
                return(objectFunction)
        time.sleep(3)


def suggest(subConfig, trial):
    """
    Suggest parameters
    """
    for keys in subConfig:
        params = {}
        for val in subConfig[keys]:
            if args.debug >= 2:
                print(f'suggest: val={val}', file=sys.stderr)
            if isinstance(val, dict):
                value = list(val.keys())[0]
            else:
                value = val
            params[value]=val
        if args.debug >= 2:
            print("suggest: keys={} params.keys={}".format(keys, list(params.keys())), file=sys.stderr)
        value = trial.suggest_categorical(keys, list(params.keys()))

        if args.debug >= 1:
            print("suggest: keys={} value={}".format(keys, value), file=sys.stderr)

        if isinstance(params[value], dict):
            subsubConfig = list(params[value].values())[0]
            suggest(subsubConfig, trial)

    return


def objective(trial):
    """
    Generate objective function
    """
    if hasattr(trial, 'number'):
        print("\ntrial.number={}".format(trial.number), file=sys.stderr)

    # Generate trial object
    suggest(config, trial)

    return submitJob(trial)


def gridSearch(study):
    """
    Enque all combination of parameters for grid search
    """
    n = 0
    for paramList in itertools.product(*config.values()):
        param = {key: paramList[i] for i, key in enumerate(config.keys())}
        if args.debug >= 1:
            print("gridSearch: param={}".format(param), file=sys.stderr)
        study.enqueue_trial(param)
        n = n+1

    return n


def defaultParameters(subConfig, study, param):
    """
    Set default parameters
    """
    for keys in subConfig:
        val = subConfig[keys][0]
        if isinstance(val, dict):
            value = list(val.keys())[0]
        else:
            value = val
        param[keys] = value
 
        if args.debug >= 1:
            print("defaultParameters: keys={} value={}".format(keys, value), file=sys.stderr)

        if isinstance(val, dict):
            subsubConfig = list(val.values())[0]
            defaultParameters(subsubConfig, study, param)

    return


def useDefaultParameters(study):
    """
    Enque default parameters
    """
    param = {}
    defaultParameters(config, study, param)
    study.enqueue_trial(param)

    return


def linkBestCase(trial):
    """
    Link best case
    """
    paramsStr = "{}".format(trial.params)
    print(paramsStr, file=sys.stderr)
    case = "_"+hashlib.sha1(paramsStr.encode('utf-8')).hexdigest()
    casePath = pathlib.Path(args.baseDir) / case
    bestPath = pathlib.Path(args.baseDir) / "best.{}".format(os.getpid())

    if bestPath.exists() and bestPath.is_symlink():
        os.remove(bestPath)
    if not bestPath.exists():
        print('Generate symblic link {} -> {}'.format(casePath,
                                                      bestPath), file=sys.stderr)
        os.symlink(case, bestPath)
    else:
        print('Warning: Unable to generate symblic link {}'.format(
            bestPath), file=sys.stderr)

    return


def main():
    """
    Main
    """
    print("Hostname: {}".format(socket.gethostname()), file=sys.stderr)
    print("PID: {}".format(os.getpid()), file=sys.stderr)

    global args
    args = parseOptions()

    global config
    config = parseConfig(args.json)

    # Create study
    if args.useStorage:
        storage = args.storage
    else:
        storage = None

    if args.prune:
        pruner = optuna.SuccessiveHalvingPruner()
    else:
        pruner = optuna.pruners.NopPruner()

    study = optuna.create_study(
        study_name=args.study,
        storage=storage,
        sampler=optuna.samplers.TPESampler(seed=args.seed),
        load_if_exists=True,
        pruner=pruner
    )

    # Use  default parameters
    if args.useGridSearch:
        args.nTrials = gridSearch(study)
    elif args.useDefaultFirst and len(study.trials) == 0:
        useDefaultParameters(study)

    # Generate base case diretory
    pathlib.Path(args.baseDir).mkdir(exist_ok=True)

    # Optimize
    study.optimize(objective, n_trials=args.nTrials, n_jobs=args.nJobs)

    # Study statistics
    pruned_trials = [t for t in study.trials if t.state ==
                     optuna.trial.TrialState.PRUNED]
    timeout_trials = [t for t in study.trials if t.state ==
                      optuna.trial.TrialState.FAIL]
    complete_trials = [t for t in study.trials if t.state ==
                       optuna.trial.TrialState.COMPLETE]

    print('\nStudy statistics: ', file=sys.stderr)
    print('  Number of finished trials: ', len(study.trials), file=sys.stderr)
    print('  Number of pruned trials: ', len(pruned_trials), file=sys.stderr)
    print('  Number of timeout  trials: ',
          len(timeout_trials), file=sys.stderr)
    print('  Number of complete trials: ', len(
        complete_trials), file=sys.stderr)

    # Best study
    trial = study.best_trial
    print('\nBest trial value: {:g}'.format(trial.value), file=sys.stderr)
    linkBestCase(trial)


if __name__ == '__main__':
    main()
