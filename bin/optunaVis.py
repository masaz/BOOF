#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import plotly
import pandas as pd
import matplotlib.pyplot as plt
import optuna
import argparse
import matplotlib as mpl


def parseOptions():
    """
    Parse options
    """
    p = argparse.ArgumentParser(
        description='Run-time bayesian optimization of solver settings for OpenFOAM solver.')

    p.add_argument('--studyName', type=str, default='BOOF')
    p.add_argument('--storage', type=str, default='sqlite:///BOOF.db')

    p.add_argument('-v', '--visualization_parameters',
                   type=str, nargs='+', default=None)
    p.add_argument('-c', '--plot_contour', action='store_true')
    p.add_argument('-o', '--plot_optimization_history', action='store_true')
    p.add_argument('-p', '--plot_parallel_coordinate', action='store_true')
    p.add_argument('-s', '--plot_slice', action='store_true')

    p.add_argument('--plot', action='store_true')
    p.add_argument('--xSize', type=float, default=8)
    p.add_argument('--ySize', type=float, default=3)
    p.add_argument('--xlimMin', type=float, nargs='+', default=[0])
    p.add_argument('--xlimMax', type=float, nargs='+', default=[100])
    p.add_argument('--ylimMin', type=float, nargs='+', default=[0])
    p.add_argument('--ylimMax', type=float, nargs='+',  default=[1])
    p.add_argument('--legendFontSize', type=int, default=16)
    p.add_argument('--xlabelFontSize', type=int, default=16)
    p.add_argument('--ylabelFontSize', type=int, default=16)
    p.add_argument('--xtickFontSize', type=int, default=16)
    p.add_argument('--ytickFontSize', type=int, default=16)
    p.add_argument('--lineWidth', type=float, default=1.5)
    p.add_argument('--topFraction', type=float, default=0.95)
    p.add_argument('--bottomFraction', type=float, default=0.2)
    p.add_argument('--rightFraction', type=float, default=0.95)
    p.add_argument('--leftFraction', type=float, default=0.1)

    return p.parse_args()


def plot(study):
    """
    Plot CPU time of trials
    """

    plt.plot(pd.DataFrame([t.value for t in study.trials]),
             linewidth=args.lineWidth, label="Trial")
    plt.plot(pd.DataFrame([t.value for t in study.trials]
                          ).cummin(), linewidth=args.lineWidth, label="Best")

    plt.subplots_adjust(
        top=args.topFraction, bottom=args.bottomFraction, right=args.rightFraction, left=args.leftFraction
    )
    plt.legend(fontsize=args.legendFontSize, loc='best')
    plt.grid()
    plt.xlabel("Trial No.", fontsize=args.xlabelFontSize)
    plt.tick_params(axis='x', labelsize=args.xtickFontSize)
    plt.tick_params(axis='y', labelsize=args.ytickFontSize)

    for i in range(len(args.xlimMin)):
        plt.xlim(args.xlimMin[i], args.xlimMax[i])
        plt.ylim(args.ylimMin[i], args.ylimMax[i])
        plotfile = "{}-{:g}-{:g}-{:g}-{:g}".format(
            args.studyName,
            args.xlimMin[i],
            args.xlimMax[i],
            args.ylimMin[i],
            args.ylimMax[i])
        plotfile = plotfile.replace(".", "_")+".pdf"
        plt.savefig(plotfile)


def main():
    """
    Main
    """
    global args
    args = parseOptions()

    study = optuna.load_study(
        study_name=args.studyName,
        storage=args.storage,
    )

    if args.plot_optimization_history:
        # visualize optimization_history
        optuna.visualization.plot_optimization_history(study)
    elif args.plot_contour:
        # Plot the parameter relationship as contour plot in a study.
        optuna.visualization.plot_contour(
            study, params=args.visualization_parameters)
    elif args.plot_parallel_coordinate:
        # Plot the high-dimentional parameter relationships in a study.
        optuna.visualization.plot_parallel_coordinate(
            study, params=args.visualization_parameters)
    elif args.plot_slice:
        # Plot the parameter relationship as slice plot in a study.
        optuna.visualization.plot_slice(
            study, params=args.visualization_parameters)
    elif args.plot:
        # plot optimization_history
        plot(study)

    pruned_trials = [t for t in study.trials if t.state ==
                     optuna.trial.TrialState.PRUNED]
    timeout_trials = [t for t in study.trials if t.state ==
                      optuna.trial.TrialState.FAIL]
    complete_trials = [t for t in study.trials if t.state ==
                       optuna.trial.TrialState.COMPLETE]

    print('\nStudy statistics: ')
    print('  Number of finished trials: ', len(study.trials))
    print('  Number of pruned trials: ', len(pruned_trials))
    print('  Number of timeout trials: ', len(timeout_trials))
    print('  Number of complete trials: ', len(complete_trials))
    trial = study.best_trial
    print('Best trial value: {:g}'.format(trial.value))


if __name__ == '__main__':
    main()
