#!/bin/bash

# Pass options for rtBOOF.py
options="$*"

# SQLite3 database name
base=BOOF
study_name=$base
db_file=$base.db
dbName=sqlite:///$db_file

# Create database
res=""
[ -f $db_file ] && res=`sqlite3 $db_file "select * from studies where study_name = '$study_name'"`
if [ -z $res ]
then
    optuna create-study --study $study_name --storage $dbName
fi

# Run-time Baysien optimization
export PYTHONUNBUFFERED=1
rtBOOF.py --useStorage $options
